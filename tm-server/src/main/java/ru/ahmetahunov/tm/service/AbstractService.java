package ru.ahmetahunov.tm.service;

import lombok.RequiredArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.IAbstractService;
import ru.ahmetahunov.tm.api.service.SqlSessionFactoryService;
import ru.ahmetahunov.tm.entity.AbstractEntity;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import java.util.List;

@RequiredArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> implements IAbstractService<T> {

    @NotNull
    private final SqlSessionFactoryService sqlSessionFactoryService;

    @NotNull
    protected SqlSession getSession() {
        return sqlSessionFactoryService.getSqlSessionFactory().openSession();
    }

    @Override
    public abstract void persist(@Nullable final T item) throws InterruptedOperationException;

    @Override
    public abstract void merge(@Nullable final T item) throws InterruptedOperationException;

    @Nullable
    @Override
    public abstract T findOne(@Nullable final String id) throws InterruptedOperationException;

    @NotNull
    @Override
    public abstract List<T> findAll() throws InterruptedOperationException;

    @Override
    public abstract void remove(@Nullable final String id) throws InterruptedOperationException;

}
