package ru.ahmetahunov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.ITaskRepository;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.api.service.SqlSessionFactoryService;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import ru.ahmetahunov.tm.util.ComparatorUtil;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    public TaskService(@NotNull final SqlSessionFactoryService sqlSessionFactoryService) {
        super(sqlSessionFactoryService);
    }

    @Override
    public void persist(@Nullable final Task task) throws InterruptedOperationException {
        if (task == null) throw new InterruptedOperationException();
        @NotNull final SqlSession session = getSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
            repository.persist(task);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public void merge(@Nullable final Task task) throws InterruptedOperationException {
        if (task == null) throw new InterruptedOperationException();
        @NotNull final SqlSession session = getSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
            repository.merge(task);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        } finally {
            session.close();
        }
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String id) throws InterruptedOperationException {
        if (id == null || id.isEmpty()) return null;
        try (@NotNull final SqlSession session = getSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            return repository.findOne(id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        }
    }

    @Nullable
    @Override
    public Task findOne(
            @Nullable final String userId,
            @Nullable final String taskId
    ) throws InterruptedOperationException {
        if (userId == null || userId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        try (@NotNull final SqlSession session = getSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            return repository.findOneById(userId, taskId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        }
    }

    @NotNull
    @Override
    public List<Task> findAll() throws InterruptedOperationException {
        try (@NotNull final SqlSession session = getSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            return repository.findAll();
        } catch (Exception e) {
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) throws InterruptedOperationException {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        try (@NotNull final SqlSession session = getSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            return repository.findAllByUserId(userId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(
            @Nullable final String userId,
            @Nullable String comparator
    ) throws InterruptedOperationException {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        try (@NotNull final SqlSession session = getSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            return repository.findAllWithComparator(userId, ComparatorUtil.getComparator(comparator));
        } catch (Exception e) {
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable String comparator
    ) throws InterruptedOperationException {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        try (@NotNull final SqlSession session = getSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            return repository.findAllByProjectId(userId, projectId, ComparatorUtil.getComparator(comparator));
        } catch (Exception e) {
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        }
    }

    @NotNull
    @Override
    public List<Task> findByName(
            @Nullable final String userId,
            @Nullable final String taskName
    ) throws InterruptedOperationException {
        if (taskName == null || taskName.isEmpty()) return Collections.emptyList();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        try (@NotNull final SqlSession session = getSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            return repository.findByName(userId, "%" + taskName + "%");
        } catch (Exception e) {
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        }
    }

    @NotNull
    @Override
    public List<Task> findByDescription(
            @Nullable final String userId,
            @Nullable final String description
    ) throws InterruptedOperationException {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (description == null || description.isEmpty()) return Collections.emptyList();
        try (@NotNull final SqlSession session = getSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            return repository.findByDescription(userId, "%" + description + "%");
        } catch (Exception e) {
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        }
    }

    @NotNull
    @Override
    public List<Task> findByNameOrDesc(
            @Nullable final String userId,
            @Nullable final String searchPhrase
    ) throws InterruptedOperationException {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (searchPhrase == null || searchPhrase.isEmpty()) return Collections.emptyList();
        try (@NotNull final SqlSession session = getSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            return repository.findByNameOrDesc(userId, "%" + searchPhrase + "%");
        } catch (Exception e) {
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        }
    }

    @Override
    public void remove(
            @Nullable final String userId,
            @Nullable final String taskId
    ) throws InterruptedOperationException {
        if (taskId == null || taskId.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        @NotNull final SqlSession session = getSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
            repository.removeById(userId, taskId);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public void remove(@Nullable final String id) throws InterruptedOperationException {
        if (id == null || id.isEmpty()) return;
        @NotNull final SqlSession session = getSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
            repository.remove(id);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws InterruptedOperationException {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final SqlSession session = getSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
            repository.removeAll(userId);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public void load(@Nullable final Collection<Task> data) throws InterruptedOperationException {
        if (data == null) return;
        @NotNull final SqlSession session = getSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
            repository.load(data);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        } finally {
            session.close();
        }
    }

}
