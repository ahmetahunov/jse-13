package ru.ahmetahunov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.ISessionRepository;
import ru.ahmetahunov.tm.api.service.ISessionService;
import ru.ahmetahunov.tm.api.service.SqlSessionFactoryService;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import ru.ahmetahunov.tm.util.SessionSignatureUtil;
import java.util.List;

public final class SessionService extends AbstractService<Session> implements ISessionService {

	public SessionService(@NotNull final SqlSessionFactoryService sqlSessionFactoryService) {
		super(sqlSessionFactoryService);
	}

	@Override
	public void persist(@Nullable final Session session) throws InterruptedOperationException {
		if (session == null) return;
		@NotNull final SqlSession sqlSession = getSession();
		@NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
		try {
			repository.persist(session);
			sqlSession.commit();
		} catch (Exception e) {
			sqlSession.rollback();
			e.printStackTrace();
			throw new InterruptedOperationException(e);
		} finally {
			sqlSession.close();
		}
	}

	@Override
	public void merge(@Nullable final Session session) throws InterruptedOperationException {
		if (session == null) return;
		@NotNull final SqlSession sqlSession = getSession();
		@NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
		try {
			repository.merge(session);
			sqlSession.commit();
		} catch (Exception e) {
			sqlSession.rollback();
			e.printStackTrace();
			throw new InterruptedOperationException(e);
		} finally {
			sqlSession.close();
		}
	}

	@Nullable
	@Override
	public Session findOne(
			@Nullable final String id,
			@Nullable final String userId
	) throws InterruptedOperationException {
		if (id == null || id.isEmpty()) return null;
		if (userId == null || userId.isEmpty()) return null;
		try (@NotNull final SqlSession sqlSession = getSession()) {
			@NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
			return repository.findOneByUserId(id, userId);
		} catch (Exception e) {
			e.printStackTrace();
			throw new InterruptedOperationException(e);
		}
	}

	@Nullable
	@Override
	public Session findOne(@Nullable final String id) throws InterruptedOperationException {
		if (id == null || id.isEmpty()) return null;
		try (@NotNull final SqlSession sqlSession = getSession()) {
			@NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
			return repository.findOne(id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new InterruptedOperationException(e);
		}
	}

	@Override
	public @NotNull List<Session> findAll() throws InterruptedOperationException {
		try (@NotNull final SqlSession sqlSession = getSession()) {
			@NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
			return repository.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			throw new InterruptedOperationException(e);
		}
	}

	@Override
	public void remove(
			@Nullable final String id,
			@Nullable final String userId
	) throws InterruptedOperationException {
		if (id == null || id.isEmpty()) return;
		if (userId == null || userId.isEmpty()) return;
		@NotNull final SqlSession sqlSession = getSession();
		@NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
		try {
			repository.removeByUserId(id, userId);
			sqlSession.commit();
		} catch (Exception e) {
			sqlSession.rollback();
			e.printStackTrace();
			throw new InterruptedOperationException(e);
		} finally {
			sqlSession.close();
		}
	}

	@Override
	public void remove(@Nullable final String id) throws InterruptedOperationException {
		if (id == null || id.isEmpty()) return;
		@NotNull final SqlSession sqlSession = getSession();
		@NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
		try {
			repository.remove(id);
			sqlSession.commit();
		} catch (Exception e) {
			sqlSession.rollback();
			e.printStackTrace();
			throw new InterruptedOperationException(e);
		} finally {
			sqlSession.close();
		}
	}

	@Override
	public void validate(@Nullable final Session session)
			throws AccessForbiddenException, InterruptedOperationException {
		if (session == null) throw new AccessForbiddenException();
		final boolean isUserIdNull = session.getUserId() == null;
		final boolean isSignatureNull = session.getSignature() == null;
		final boolean isRoleNull = session.getRole() == null;
		if (isUserIdNull || isSignatureNull || isRoleNull)
			throw new AccessForbiddenException();
		try (@NotNull final SqlSession sqlSession = getSession()) {
			@NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
			@Nullable final Session checkSession = repository.findOne(session.getId());
			if (checkSession == null || checkSession.getSignature() == null)
				throw new AccessForbiddenException();
			@Nullable final String signature = session.getSignature();
			session.setSignature(null);
			@Nullable final String hash = SessionSignatureUtil.sign(session);
			if (!checkSession.getSignature().equals(hash) || !signature.equals(hash))
				throw new AccessForbiddenException();
			final long existTime = System.currentTimeMillis() - session.getTimestamp();
			if (existTime > 32400000) {
				repository.remove(session.getId());
				throw new AccessForbiddenException("Access denied! Session has expired.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new InterruptedOperationException(e);
		}
	}

	@Override
	public void validate(
			@Nullable final Session session,
			@NotNull final Role role
	) throws AccessForbiddenException, InterruptedOperationException {
		validate(session);
		if (!role.equals(session.getRole())) throw new AccessForbiddenException();
	}

}
