package ru.ahmetahunov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IUserRepository;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.api.service.SqlSessionFactoryService;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import ru.ahmetahunov.tm.util.PassUtil;
import java.util.Collection;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    public UserService(@NotNull final SqlSessionFactoryService sqlSessionFactoryService) {
        super(sqlSessionFactoryService);
    }

    @Override
    public void persist(@Nullable final User user) throws InterruptedOperationException {
        if (user == null) return;
        if (user.getLogin().isEmpty()) throw new InterruptedOperationException();
        @NotNull final SqlSession session = getSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        try {
            repository.persist(user);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public void merge(@Nullable final User user) throws InterruptedOperationException {
        if (user == null) return;
        @NotNull final SqlSession session = getSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        try {
            repository.merge(user);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        } finally {
            session.close();
        }
    }

    @Nullable
    @Override
    public User findUser(@Nullable final String login) throws InterruptedOperationException {
        if (login == null || login.isEmpty()) return null;
        try (@NotNull final SqlSession session = getSession()) {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            return repository.findUser(login);
        } catch (Exception e) {
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        }
    }

    @Nullable
    @Override
    public User findOne(@Nullable final String id) throws InterruptedOperationException {
        if (id == null || id.isEmpty()) return null;
        try (@NotNull final SqlSession session = getSession()) {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            return repository.findOne(id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        }
    }

    @NotNull
    @Override
    public List<User> findAll() throws InterruptedOperationException {
        try (@NotNull final SqlSession session = getSession()) {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            return repository.findAll();
        } catch (Exception e) {
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        }
    }

    @Override
    public void updatePasswordAdmin(@Nullable final String userId, @Nullable final String password)
            throws InterruptedOperationException {
        if (userId == null || userId.isEmpty())
            throw new InterruptedOperationException("This user does not exist.");
        @NotNull final SqlSession session = getSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        try {
            @Nullable final User user = repository.findOne(userId);
            if (user == null) throw new InterruptedOperationException("This user does not exist.");
            @NotNull final String hash = PassUtil.getHash(password);
            user.setPassword(hash);
            repository.merge(user);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public void updateRole(@Nullable final String userId, @Nullable final Role role)
            throws InterruptedOperationException {
        if (userId == null || userId.isEmpty()) throw new InterruptedOperationException("This user does not exist.");
        if (role == null) throw new InterruptedOperationException("Unknown role.");
        @NotNull final SqlSession session = getSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        try {
            @Nullable final User user = repository.findOne(userId);
            if (user == null) throw new InterruptedOperationException("This user does not exist.");
            user.setRole(role);
            repository.merge(user);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public void updatePassword(
            @Nullable final String userId,
            @Nullable final String oldPassword,
            @Nullable final String password
    ) throws AccessForbiddenException, InterruptedOperationException {
        if (userId == null || userId.isEmpty()) throw new AccessForbiddenException("This user does not exist.");
        if (oldPassword == null || oldPassword.isEmpty()) throw new AccessForbiddenException("Wrong password.");
        if (password == null || password.isEmpty()) throw new AccessForbiddenException("Wrong password.");
        @NotNull final SqlSession session = getSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        try {
            @Nullable final User user = repository.findOne(userId);
            if (user == null) throw new InterruptedOperationException("This user does not exist.");
            @NotNull final String oldHash = PassUtil.getHash(oldPassword);
            if (!user.getPassword().equals(oldHash)) throw new AccessForbiddenException("Wrong password.");
            @NotNull final String hash = PassUtil.getHash(password);
            user.setPassword(hash);
            repository.merge(user);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public void updateLogin(
            @Nullable final String userId,
            @Nullable final String login
    ) throws AccessForbiddenException, InterruptedOperationException {
        if (login == null || login.isEmpty()) throw new InterruptedOperationException("Wrong format login.");
        if (userId == null || userId.isEmpty()) throw new AccessForbiddenException("This user does not exist.");
        @NotNull final SqlSession session = getSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        try {
            @Nullable final User user = repository.findOne(userId);
            if (user == null) throw new InterruptedOperationException("This user does not exist.");
            user.setLogin(login);
            repository.merge(user);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public void remove(@Nullable final String id) throws InterruptedOperationException {
        if (id == null || id.isEmpty()) throw new InterruptedOperationException();
        @NotNull final SqlSession session = getSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        try {
            repository.remove(id);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public void load(@Nullable final Collection<User> data) throws InterruptedOperationException {
        if (data == null) return;
        @NotNull final SqlSession session = getSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        try {
            repository.load(data);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public boolean contains(@Nullable final String login) throws InterruptedOperationException {
        if (login == null || login.isEmpty()) return true;
        @Nullable final User user = findUser(login);
        return user != null;
    }

}
