package ru.ahmetahunov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IProjectRepository;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.SqlSessionFactoryService;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import ru.ahmetahunov.tm.util.ComparatorUtil;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    public ProjectService(@NotNull final SqlSessionFactoryService sqlSessionFactoryService) {
        super(sqlSessionFactoryService);
    }

    @Override
    public void persist(@Nullable final Project project) throws InterruptedOperationException {
        if (project == null) throw new InterruptedOperationException();
        if (project.getName().isEmpty()) throw new InterruptedOperationException();
        @NotNull final SqlSession session = getSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        try {
            repository.persist(project);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public void merge(@Nullable final Project project) throws InterruptedOperationException {
        if (project == null) throw new InterruptedOperationException();
        if (project.getName().isEmpty()) throw new InterruptedOperationException();
        @NotNull final SqlSession session = getSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        try {
            repository.merge(project);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        } finally {
            session.close();
        }
    }

    @Nullable
    @Override
    public Project findOne(
            @Nullable final String userId,
            @Nullable final String id
    ) throws InterruptedOperationException {
        if (id == null || id.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        try (@NotNull final SqlSession session = getSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
           return repository.findOneByName(userId, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        }
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id) throws InterruptedOperationException {
        if (id == null || id.isEmpty()) return null;
        try (@NotNull final SqlSession session = getSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            return repository.findOne(id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        }
    }

    @Override
    public @NotNull List<Project> findAll() throws InterruptedOperationException {
        try (@NotNull final SqlSession session = getSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            return repository.findAll();
        } catch (Exception e) {
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) throws InterruptedOperationException {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        try (@NotNull final SqlSession session = getSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            return repository.findAllByUserId(userId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(
            @Nullable final String userId,
            @Nullable String comparator
    ) throws InterruptedOperationException {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        try (@NotNull final SqlSession session = getSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            return repository.findAllWithComparator(userId, ComparatorUtil.getComparator(comparator));
        } catch (Exception e) {
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        }
    }

    @NotNull
    @Override
    public List<Project> findByName(
            @Nullable final String userId,
            @Nullable final String projectName
    ) throws InterruptedOperationException {
        if (projectName == null || projectName.isEmpty()) return Collections.emptyList();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        try (@NotNull final SqlSession session = getSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            return repository.findByName(userId, "%" + projectName + "%");
        } catch (Exception e) {
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        }
    }

    @NotNull
    @Override
    public List<Project> findByDescription(
            @Nullable final String userId,
            @Nullable final String description
    ) throws InterruptedOperationException {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (description == null || description.isEmpty()) return Collections.emptyList();
        try (@NotNull final SqlSession session = getSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            return repository.findByDescription(userId, "%" + description + "%");
        } catch (Exception e) {
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        }
    }

    @NotNull
    @Override
    public List<Project> findByNameOrDesc(
            @Nullable final String userId,
            @Nullable final String searchPhrase
    ) throws InterruptedOperationException {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (searchPhrase == null || searchPhrase.isEmpty()) return Collections.emptyList();
        try (@NotNull final SqlSession session = getSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            return repository.findByNameOrDesc(userId, "%" + searchPhrase + "%");
        } catch (Exception e) {
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        }
    }

    @Override
    public void remove(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws InterruptedOperationException {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        @NotNull final SqlSession session = getSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        try {
            repository.remove(userId, projectId);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public void remove(@Nullable final String id) throws InterruptedOperationException {
        if (id == null || id.isEmpty()) return;
        @NotNull final SqlSession session = getSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        try {
            repository.removeById(id);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws InterruptedOperationException {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final SqlSession session = getSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        try {
            repository.removeAll(userId);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public void load(@Nullable final Collection<Project> data) throws InterruptedOperationException {
        if (data == null) return;
        @NotNull final SqlSession session = getSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        try {
            repository.load(data);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
            throw new InterruptedOperationException(e);
        } finally {
            session.close();
        }
    }

}
