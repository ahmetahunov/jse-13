package ru.ahmetahunov.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.repository.*;
import ru.ahmetahunov.tm.api.service.*;
import ru.ahmetahunov.tm.constant.AppConst;
import ru.ahmetahunov.tm.endpoint.*;
import ru.ahmetahunov.tm.entity.Property;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import ru.ahmetahunov.tm.service.*;
import ru.ahmetahunov.tm.util.PassUtil;
import javax.sql.DataSource;
import javax.xml.ws.Endpoint;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator, SqlSessionFactoryService, IPropertyService {

    @NotNull
    private final Map<String, Property> propertyMap = new HashMap<>();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(this);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(this);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(this);

    @Getter
    @NotNull
    private final IDataService dataService = new DataService(this);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(this);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointImpl(this);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpointImpl(this);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpointImpl(this);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpointImpl(this);

    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpointImpl(this);

    @Getter
    @Nullable
    private SqlSessionFactory sqlSessionFactory;

    public void init() throws Exception {
        loadProperties();
        prepareDB();
        prepareSqlSessionFactory();
        createDefaultUsers();
        startServer();
    }

    private void loadProperties() throws IOException {
        @NotNull final Properties properties = new Properties();
        @NotNull final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        @Nullable final InputStream fis = classLoader.getResourceAsStream("application.properties");
        properties.load(fis);
        @NotNull final Set<String> names = properties.stringPropertyNames();
        for (@NotNull final String name : names) {
            @NotNull final Property property = new Property();
            property.setName(name);
            property.setValue(properties.getProperty(name));
            propertyMap.put(property.getName(), property);
        }
    }

    private void prepareDB() throws Exception {
        @NotNull final String url = propertyMap.get(AppConst.DB_START_HOST).getValue();
        @NotNull final String userName = propertyMap.get(AppConst.DB_LOGIN).getValue();
        @NotNull final String password = propertyMap.get(AppConst.DB_PASSWORD).getValue();
        Class.forName(propertyMap.get(AppConst.DB_DRIVER).getValue());
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, password);
        @Nullable final Statement st = connection.createStatement();
        if (st == null) throw new InterruptedOperationException("Prepare db failed.");
        st.execute(propertyMap.get(AppConst.DB_CREATE).getValue());
        st.execute(propertyMap.get(AppConst.DB_USE).getValue());
        st.execute(propertyMap.get(AppConst.USER_TABLE).getValue());
        st.execute(propertyMap.get(AppConst.SESSION_TABLE).getValue());
        st.execute(propertyMap.get(AppConst.PROJECT_TABLE).getValue());
        st.execute(propertyMap.get(AppConst.TASK_TABLE).getValue());
        st.close();
        System.out.println("Database ready");
    }

    private void prepareSqlSessionFactory() {
        @NotNull final SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        @NotNull final String driver = propertyMap.get(AppConst.DB_DRIVER).getValue();
        @NotNull final String host = propertyMap.get(AppConst.DB_HOST).getValue();
        @NotNull final String name = propertyMap.get(AppConst.DB_LOGIN).getValue();
        @NotNull final String password = propertyMap.get(AppConst.DB_PASSWORD).getValue();
        @NotNull final DataSource dataSource = new PooledDataSource(driver, host, name, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("Develop", transactionFactory, dataSource);
        @NotNull final Configuration config = new Configuration(environment);
        config.addMapper(IUserRepository.class);
        config.addMapper(ITaskRepository.class);
        config.addMapper(ISessionRepository.class);
        config.addMapper(IProjectRepository.class);
        this.sqlSessionFactory = builder.build(config);
    }

    private void createDefaultUsers() throws AccessForbiddenException, InterruptedOperationException, SQLException {
        if (!userService.contains("user")) {
            @NotNull final User user = new User();
            user.setId("64a8bae7-e916-4d1d-9b1e-04bc19d0bd91");
            user.setLogin(propertyMap.get(AppConst.USER_LOGIN).getValue());
            user.setPassword(PassUtil.getHash(propertyMap.get(AppConst.USER_PASSWORD).getValue()));
            userService.persist(user);
        }
        if (!userService.contains("admin")) {
            @NotNull final User admin = new User();
            admin.setId("3d8043c1-6aeb-4df9-af8d-7a8de0f99a09");
            admin.setLogin(propertyMap.get(AppConst.ADMIN_LOGIN).getValue());
            admin.setPassword(PassUtil.getHash(propertyMap.get(AppConst.ADMIN_PASSWORD).getValue()));
            admin.setRole(Role.ADMINISTRATOR);
            userService.persist(admin);
        }
    }

    private void startServer() {
        Endpoint.publish(AppConst.USER_ENDPOINT, userEndpoint);
        System.out.println(AppConst.USER_ENDPOINT + " is running...");
        Endpoint.publish(AppConst.SESSION_ENDPOINT, sessionEndpoint);
        System.out.println(AppConst.SESSION_ENDPOINT + " is running...");
        Endpoint.publish(AppConst.TASK_ENDPOINT, taskEndpoint);
        System.out.println(AppConst.TASK_ENDPOINT + " is running...");
        Endpoint.publish(AppConst.PROJECT_ENDPOINT, projectEndpoint);
        System.out.println(AppConst.PROJECT_ENDPOINT + " is running...");
        Endpoint.publish(AppConst.ADMIN_ENDPOINT, adminEndpoint);
        System.out.println(AppConst.ADMIN_ENDPOINT + " is running...");
    }

    @NotNull
    @Override
    public String getSecretPhrase() {
        return propertyMap.get(AppConst.SECRET_PHRASE).getValue();
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return this;
    }

}
