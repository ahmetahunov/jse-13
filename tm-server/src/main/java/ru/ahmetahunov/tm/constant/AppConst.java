package ru.ahmetahunov.tm.constant;

public final class AppConst {

	public static final String SECRET_PHRASE = "secret";

	public static final String SALT = "salt";

	public static final String SALT_P = "lfmao125ak-i";

	public static final int CYCLE = 33;

	public static final int CYCLE_P = 10;

	public static final String ADMIN_LOGIN = "default.admin";

	public static final String ADMIN_PASSWORD = "default.admin.password";

	public static final String USER_LOGIN = "default.user";

	public static final String USER_PASSWORD = "default.user.password";

	public static final String DB_START_HOST = "db.startHost";

	public static final String DB_HOST = "db.host";

	public static final String DB_LOGIN = "db.login";

	public static final String DB_PASSWORD = "db.password";

	public static final String DB_DRIVER = "db.driver";

	public static final String DB_CREATE = "databaseCreate";

	public static final String DB_USE = "databaseUse";

	public static final String USER_TABLE = "createTableUser";

	public static final String PROJECT_TABLE = "createTableProject";

	public static final String TASK_TABLE = "createTableTask";

	public static final String SESSION_TABLE = "createTableSession";

	public static final String USER_ENDPOINT = "http://localhost:8080/UserEndpoint?wsdl";

	public static final String PROJECT_ENDPOINT = "http://localhost:8080/ProjectEndpoint?wsdl";

	public static final String TASK_ENDPOINT = "http://localhost:8080/TaskEndpoint?wsdl";

	public static final String SESSION_ENDPOINT = "http://localhost:8080/SessionEndpoint?wsdl";

	public static final String ADMIN_ENDPOINT = "http://localhost:8080/AdminEndpoint?wsdl";

}
