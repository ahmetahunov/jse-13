package ru.ahmetahunov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.ProjectEndpoint;
import ru.ahmetahunov.tm.api.service.IPropertyService;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import ru.ahmetahunov.tm.util.CipherUtil;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.ahmetahunov.tm.api.endpoint.ProjectEndpoint")
public final class ProjectEndpointImpl implements ProjectEndpoint {

	private ServiceLocator serviceLocator;

	public ProjectEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	@Nullable
	@Override
	@WebMethod
	public Project createProject(
			@WebParam(name = "session") final String token,
			@WebParam(name = "project") final Project project
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session);
		if (project != null) project.setUserId(session.getUserId());
		serviceLocator.getProjectService().persist(project);
		return serviceLocator.getProjectService().findOne(session.getUserId(), project.getId());
	}

	@Nullable
	@Override
	@WebMethod
	public Project updateProject(
			@WebParam(name = "session") final String token,
			@WebParam(name = "project") final Project project
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session);
		if (project != null) project.setUserId(session.getUserId());
		serviceLocator.getProjectService().merge(project);
		return serviceLocator.getProjectService().findOne(session.getUserId(), project.getId());
	}

	@Nullable
	@Override
	@WebMethod
	public Project findOneProject(
			@WebParam(name = "session") final String token,
			@WebParam(name = "projectId") final String projectId
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getProjectService().findOne(session.getUserId(), projectId);
	}

	@NotNull
	@Override
	@WebMethod
	public List<Project> findProjectByName(
			@WebParam(name = "session") final String token,
			@WebParam(name = "projectName") final String projectName
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getProjectService().findByName(session.getUserId(), projectName);
	}

	@NotNull
	@Override
	@WebMethod
	public List<Project> findProjectByDescription(
			@WebParam(name = "session") final String token,
			@WebParam(name = "description") final String description
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getProjectService().findByDescription(session.getUserId(), description);
	}

	@NotNull
	@Override
	@WebMethod
	public List<Project> findProjectByNameOrDesc(
			@WebParam(name = "session") final String token,
			@WebParam(name = "searchPhrase") final String searchPhrase
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getProjectService().findByNameOrDesc(session.getUserId(), searchPhrase);
	}

	@NotNull
	@Override
	@WebMethod
	public List<Project> findAllProjects(
			@WebParam(name = "session") final String token,
			@WebParam(name = "comparatorName") final String comparatorName
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getProjectService().findAll(session.getUserId(), comparatorName);
	}

	@Override
	@WebMethod
	public void removeAllProjects(
			@WebParam(name = "session") final String token
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session);
		serviceLocator.getProjectService().removeAll(session.getUserId());
	}

	@Override
	@WebMethod
	public void removeProject(
			@WebParam(name = "session") final String token,
			@WebParam(name = "projectId") final String projectId
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session);
		serviceLocator.getProjectService().remove(session.getUserId(), projectId);
	}

}
