package ru.ahmetahunov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.AdminEndpoint;
import ru.ahmetahunov.tm.api.service.IPropertyService;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import ru.ahmetahunov.tm.util.CipherUtil;
import ru.ahmetahunov.tm.util.PassUtil;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.io.IOException;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.ahmetahunov.tm.api.endpoint.AdminEndpoint")
public final class AdminEndpointImpl implements AdminEndpoint {

	private ServiceLocator serviceLocator;

	public AdminEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	@Override
	@WebMethod
	public void dataSaveBin(
			@WebParam(name = "session") final String token
	) throws InterruptedOperationException, AccessForbiddenException, IOException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		serviceLocator.getDataService().dataSaveBin();
	}

	@Override
	@WebMethod
	public void dataSaveXmlJaxb(
			@WebParam(name = "session") final String token
	) throws AccessForbiddenException, IOException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		serviceLocator.getDataService().dataSaveXmlJaxb();
	}

	@Override
	@WebMethod
	public void dataSaveXmlJackson(
			@WebParam(name = "session") final String token
	) throws AccessForbiddenException, IOException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		serviceLocator.getDataService().dataSaveXmlJackson();
	}

	@Override
	@WebMethod
	public void dataSaveJsonJaxb(
			@WebParam(name = "session") final String token
	) throws AccessForbiddenException, IOException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		serviceLocator.getDataService().dataSaveJsonJaxb();
	}

	@Override
	@WebMethod
	public void dataSaveJsonJackson(
			@WebParam(name = "session") final String token
	) throws AccessForbiddenException, IOException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		serviceLocator.getDataService().dataSaveJsonJackson();
	}

	@Override
	@WebMethod
	public void dataLoadBin(
			@WebParam(name = "session") final String token
	) throws AccessForbiddenException, InterruptedOperationException, IOException, ClassNotFoundException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		serviceLocator.getDataService().dataLoadBin();
	}

	@Override
	@WebMethod
	public void dataLoadXmlJaxb(
			@WebParam(name = "session") final String token
	) throws AccessForbiddenException, InterruptedOperationException, IOException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		serviceLocator.getDataService().dataLoadXmlJaxb();
	}

	@Override
	@WebMethod
	public void dataLoadXmlJackson(
			@WebParam(name = "session") final String token
	) throws AccessForbiddenException, IOException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		serviceLocator.getDataService().dataLoadXmlJackson();
	}

	@Override
	@WebMethod
	public void dataLoadJsonJaxb(
			@WebParam(name = "session") final String token
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		serviceLocator.getDataService().dataLoadJsonJaxb();
	}

	@Override
	@WebMethod
	public void dataLoadJsonJackson(
			@WebParam(name = "session") final String token
	) throws AccessForbiddenException, InterruptedOperationException, IOException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		serviceLocator.getDataService().dataLoadJsonJackson();
	}

	@NotNull
	@Override
	@WebMethod
	public List<User> findAllUsers(
			@WebParam(name = "session") final String token
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		return serviceLocator.getUserService().findAll();
	}

	@Override
	@WebMethod
	public void userUpdatePasswordAdmin(
			@WebParam(name = "session") final String token,
			@WebParam(name = "userId") final String userId,
			@WebParam(name = "password") final String password
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		serviceLocator.getUserService().updatePasswordAdmin(userId, password);
	}

	@Override
	@WebMethod
	public void userChangeRoleAdmin(
			@WebParam(name = "session") final String token,
			@WebParam(name = "userId") final String userId,
			@WebParam(name = "role") final Role role
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		serviceLocator.getUserService().updateRole(userId, role);
	}

	@Override
	@WebMethod
	public void userRemove(
			@WebParam(name = "session") final String token,
			@WebParam(name = "userId") final String userId
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		serviceLocator.getUserService().remove(userId);
	}

	@Nullable
	@Override
	@WebMethod
	public User userRegisterAdmin(
			@WebParam(name = "session") final String token,
			@WebParam(name = "user") final String login,
			@WebParam(name = "password") final String password
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		@NotNull final User user = new User();
		user.setLogin(login);
		user.setPassword(PassUtil.getHash(password));
		serviceLocator.getUserService().persist(user);
		return serviceLocator.getUserService().findOne(user.getId());
	}

	@Nullable
	@Override
	@WebMethod
	public User userFindByLogin(
			@WebParam(name = "session") final String token,
			@WebParam(name = "login") final String login
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session, Role.ADMINISTRATOR);
		return serviceLocator.getUserService().findUser(login);
	}

}
