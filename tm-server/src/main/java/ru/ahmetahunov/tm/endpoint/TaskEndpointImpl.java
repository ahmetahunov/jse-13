package ru.ahmetahunov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.TaskEndpoint;
import ru.ahmetahunov.tm.api.service.IPropertyService;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import ru.ahmetahunov.tm.util.CipherUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.ahmetahunov.tm.api.endpoint.TaskEndpoint")
public final class TaskEndpointImpl implements TaskEndpoint {

	private ServiceLocator serviceLocator;

	public TaskEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	@Nullable
	@Override
	@WebMethod
	public Task createTask(
			@WebParam(name = "session") final String token,
			@WebParam(name = "task") final Task task
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session);
		if (task != null) task.setUserId(session.getUserId());
		serviceLocator.getTaskService().persist(task);
		return serviceLocator.getTaskService().findOne(session.getUserId(), task.getId());
	}

	@Nullable
	@Override
	@WebMethod
	public Task updateTask(
			@WebParam(name = "session") final String token,
			@WebParam(name = "task") final Task task
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session);
		if (task != null) task.setUserId(session.getUserId());
		serviceLocator.getTaskService().merge(task);
		return serviceLocator.getTaskService().findOne(session.getUserId(), task.getId());
	}

	@NotNull
	@Override
	@WebMethod
	public List<Task> findAllTasks(
			@WebParam(name = "session") final String token,
			@WebParam(name = "comparatorName") final String comparatorName
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getTaskService().findAll(session.getUserId(), comparatorName);
	}

	@NotNull
	@Override
	@WebMethod
	public List<Task> findAllProjectTasks(
			@WebParam(name = "session") final String token,
			@WebParam(name = "projectId") final String projectId
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getTaskService().findAll(session.getUserId(), projectId, "name");
	}

	@Nullable
	@Override
	@WebMethod
	public Task findOneTask(
			@WebParam(name = "session") final String token,
			@WebParam(name = "taskId") final String taskId
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getTaskService().findOne(session.getUserId(), taskId);
	}

	@NotNull
	@Override
	@WebMethod
	public List<Task> findTaskByName(
			@WebParam(name = "session") final String token,
			@WebParam(name = "taskName") final String taskName
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getTaskService().findByName(session.getUserId(), taskName);
	}

	@NotNull
	@Override
	@WebMethod
	public List<Task> findTaskByDescription(
			@WebParam(name = "session") final String token,
			@WebParam(name = "description") final String description
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getTaskService().findByDescription(session.getUserId(), description);
	}

	@NotNull
	@Override
	@WebMethod
	public List<Task> findTaskByNameOrDesc(
			@WebParam(name = "session") final String token,
			@WebParam(name = "searchPhrase") final String searchPhrase
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getTaskService().findByNameOrDesc(session.getUserId(), searchPhrase);
	}

	@Override
	@WebMethod
	public void removeAllTasks(
			@WebParam(name = "session") final String token
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session);
		serviceLocator.getTaskService().removeAll(session.getUserId());
	}

	@Override
	@WebMethod
	public void removeTask(
			@WebParam(name = "session") final String token,
			@WebParam(name = "taskId") final String taskId
	) throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
		@Nullable final Session session = CipherUtil.decrypt(token, propertyService.getSecretPhrase());
		serviceLocator.getSessionService().validate(session);
		serviceLocator.getTaskService().remove(session.getUserId(), taskId);
	}

}
