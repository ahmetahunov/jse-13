package ru.ahmetahunov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.enumerated.Role;
import javax.xml.bind.annotation.XmlType;

@Getter
@Setter
@XmlType
@NoArgsConstructor
public final class Session extends AbstractEntity {

	@Nullable
	private String userId;

	@Nullable
	private String signature;

	@Nullable
	private Role role;

	private long timestamp = System.currentTimeMillis();

}
