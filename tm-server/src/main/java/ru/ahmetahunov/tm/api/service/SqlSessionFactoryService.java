package ru.ahmetahunov.tm.api.service;

import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

public interface SqlSessionFactoryService {

	@NotNull
	public SqlSessionFactory getSqlSessionFactory();

}
