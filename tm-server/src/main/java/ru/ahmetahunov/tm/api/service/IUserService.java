package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import java.util.Collection;

public interface IUserService extends IAbstractService<User> {

    @Nullable
    public User findUser(String login) throws InterruptedOperationException;

    public void updatePasswordAdmin(String userId, String password) throws InterruptedOperationException, AccessForbiddenException;

    public void updateRole(String userId, Role role) throws InterruptedOperationException;

    public void updatePassword(String userId, String oldPassword, String newPassword) throws AccessForbiddenException, InterruptedOperationException;

    public void updateLogin(String userId, String login) throws AccessForbiddenException, InterruptedOperationException;

    public void load(Collection<User> data) throws InterruptedOperationException;

    public boolean contains(String login) throws InterruptedOperationException;

}
