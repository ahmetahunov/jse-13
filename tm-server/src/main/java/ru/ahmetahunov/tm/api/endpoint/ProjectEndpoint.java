package ru.ahmetahunov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ProjectEndpoint {

	@Nullable
	@WebMethod
	public Project createProject(
			@WebParam(name = "token") String token,
			@WebParam(name = "project") Project project
	) throws AccessForbiddenException, InterruptedOperationException;

	@Nullable
	@WebMethod
	public Project updateProject(
			@WebParam(name = "token") String token,
			@WebParam(name = "project") Project project
	) throws AccessForbiddenException, InterruptedOperationException;

	@Nullable
	@WebMethod
	public Project findOneProject(
			@WebParam(name = "token") String token,
			@WebParam(name = "projectId") String projectId
	) throws AccessForbiddenException, InterruptedOperationException;

	@NotNull
	@WebMethod
	public List<Project> findProjectByName(
			@WebParam(name = "token") String token,
			@WebParam(name = "projectName") String projectName
	) throws AccessForbiddenException, InterruptedOperationException;

	@NotNull
	@WebMethod
	public List<Project> findProjectByDescription(
			@WebParam(name = "token") String token,
			@WebParam(name = "description") String description
	) throws AccessForbiddenException, InterruptedOperationException;

	@NotNull
	@WebMethod
	public List<Project> findProjectByNameOrDesc(
			@WebParam(name = "token") String token,
			@WebParam(name = "searchPhrase") String searchPhrase
	) throws AccessForbiddenException, InterruptedOperationException;

	@NotNull
	@WebMethod
	public List<Project> findAllProjects(
			@WebParam(name = "token") String token,
			@WebParam(name = "comparator") String comparator
	) throws AccessForbiddenException, InterruptedOperationException;

	@WebMethod
	public void removeAllProjects(
			@WebParam(name = "token") String token
	) throws AccessForbiddenException, InterruptedOperationException;

	@WebMethod
	public void removeProject(
			@WebParam(name = "token") String token,
			@WebParam(name = "projectId") String projectId
	) throws AccessForbiddenException, InterruptedOperationException;

}
