package ru.ahmetahunov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.User;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO app_user (id, login, password, role) VALUES (#{id}, #{login}, #{password}, #{role})")
    public void persist(@NotNull User user) throws SQLException;

    @Update("UPDATE app_user SET login = #{login}, password = #{password}, role = #{role} WHERE id = #{id}")
    public void merge(@NotNull User user) throws SQLException;

    @Nullable
    @Select("SELECT * FROM app_user WHERE id = #{id}")
    public User findOne(@NotNull String id) throws SQLException;

    @Nullable
    @Select("SELECT * FROM app_user WHERE login = #{login}")
    public User findUser(@NotNull String login) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_user")
    public List<User> findAll() throws SQLException;

    @Delete("DELETE FROM app_user WHERE id = #{id}")
    public void remove(@NotNull String id) throws SQLException;

    @Delete("DELETE FROM app_user")
    public void removeAll() throws SQLException;

    @Insert({
            "<script>",
            "INSERT IGNORE INTO app_user",
            "values ",
            "<foreach  collection='data' item='user' separator=','>",
            "(#{user.id}, #{user.login}, #{user.password}, #{user.role})",
            "</foreach>",
            "</script>"
    })
    public void load(@NotNull @Param("data") Collection<User> data) throws SQLException;

}
