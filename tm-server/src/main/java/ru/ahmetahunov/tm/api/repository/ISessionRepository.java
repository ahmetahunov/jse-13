package ru.ahmetahunov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Session;
import java.sql.SQLException;
import java.util.List;

public interface ISessionRepository {

	@Insert("INSERT INTO app_session (id, user_id, signature, role, timestamp) " +
			"VALUES (#{id}, #{userId}, #{signature}, #{role}, #{timestamp})")
	public void persist(@NotNull Session session) throws SQLException;

	@Update("UPDATE app_session SET user_id = #{userId}, signature = #{signature}," +
			" role = #{role}, timestamp = #{timestamp} WHERE id = #{id}")
	public void merge(@NotNull Session session) throws SQLException;

	@Nullable
	@Result(property = "userId", column = "user_id")
	@Select("SELECT * FROM app_session WHERE id = #{id}")
	public Session findOne(@NotNull @Param("id") String id) throws SQLException;

	@Nullable
	@Result(property = "userId", column = "user_id")
	@Select("SELECT * FROM app_session WHERE user_id = #{userId} AND id = #{id}")
	public Session findOneByUserId(
			@NotNull @Param("id") String id,
			@NotNull @Param("userId") String userId
	) throws SQLException;

	@NotNull
	@Select("SELECT * FROM app_session")
	@Result(property = "userId", column = "user_id")
	public List<Session> findAll() throws SQLException;

	@Delete("DELETE FROM app_session WHERE id = #{id}")
	public void remove(@NotNull @Param("id") String id) throws SQLException;

	@Delete("DELETE FROM app_session WHERE id = #{id} AND user_id = #{userId}")
	public void removeByUserId(
			@NotNull @Param("id") String id,
			@NotNull @Param("userId") String userId
	) throws SQLException;

}
