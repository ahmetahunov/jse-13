package ru.ahmetahunov.tm.api.service;

import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import java.io.IOException;

public interface IDataService {

	public void dataSaveBin() throws IOException, InterruptedOperationException;

	public void dataSaveXmlJaxb() throws IOException, InterruptedOperationException;

	public void dataSaveXmlJackson() throws IOException, InterruptedOperationException;

	public void dataSaveJsonJaxb() throws IOException, InterruptedOperationException;

	public void dataSaveJsonJackson() throws IOException, InterruptedOperationException;

	public void dataLoadBin() throws InterruptedOperationException, IOException, ClassNotFoundException;

	public void dataLoadXmlJaxb() throws InterruptedOperationException, IOException;

	public void dataLoadXmlJackson() throws InterruptedOperationException, IOException;

	public void dataLoadJsonJaxb() throws InterruptedOperationException;

	public void dataLoadJsonJackson() throws InterruptedOperationException, IOException;

}
