package ru.ahmetahunov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.io.IOException;
import java.util.List;

@WebService
public interface AdminEndpoint {

	@WebMethod
	public void dataSaveBin(
			@WebParam(name = "token") String token
	) throws AccessForbiddenException, IOException, InterruptedOperationException;

	@WebMethod
	public void dataSaveXmlJaxb(
			@WebParam(name = "token") String token
	) throws AccessForbiddenException, IOException, InterruptedOperationException;

	@WebMethod
	public void dataSaveXmlJackson(
			@WebParam(name = "token") String token
	) throws AccessForbiddenException, IOException, InterruptedOperationException;

	@WebMethod
	public void dataSaveJsonJaxb(
			@WebParam(name = "token") String token
	) throws AccessForbiddenException, IOException, InterruptedOperationException;

	@WebMethod
	public void dataSaveJsonJackson(
			@WebParam(name = "token") String token
	) throws AccessForbiddenException, IOException, InterruptedOperationException;

	@WebMethod
	public void dataLoadBin(
			@WebParam(name = "token") String token
	) throws AccessForbiddenException, InterruptedOperationException, IOException, ClassNotFoundException;

	@WebMethod
	public void dataLoadXmlJaxb(
			@WebParam(name = "token") String token
	) throws AccessForbiddenException, InterruptedOperationException, IOException;

	@WebMethod
	public void dataLoadXmlJackson(
			@WebParam(name = "token") String token
	) throws AccessForbiddenException, IOException, InterruptedOperationException;

	@WebMethod
	public void dataLoadJsonJaxb(
			@WebParam(name = "token") String token
	) throws AccessForbiddenException, InterruptedOperationException;

	@WebMethod
	public void dataLoadJsonJackson(
			@WebParam(name = "token") String token
	) throws AccessForbiddenException, InterruptedOperationException, IOException;

	@NotNull
	@WebMethod
	public List<User> findAllUsers(
			@WebParam(name = "token") String token
	) throws AccessForbiddenException, InterruptedOperationException;

	@WebMethod
	void userUpdatePasswordAdmin(
			@WebParam(name = "token") String token,
			@WebParam(name = "userId") String userId,
			@WebParam(name = "password") String password
	) throws AccessForbiddenException, InterruptedOperationException;

	@Nullable
	@WebMethod
	public User userRegisterAdmin(
			@WebParam(name = "token") String token,
			@WebParam(name = "user") String login,
			@WebParam(name = "password") String password
	) throws AccessForbiddenException, InterruptedOperationException;

	@Nullable
	@WebMethod
	public User userFindByLogin(
			@WebParam(name = "token") String token,
			@WebParam(name = "login") String login
	) throws AccessForbiddenException, InterruptedOperationException;

	@WebMethod
	public void userChangeRoleAdmin(
			@WebParam(name = "token") String token,
			@WebParam(name = "userId") String userId,
			@WebParam(name = "role") Role role
	) throws AccessForbiddenException, InterruptedOperationException;

	@WebMethod
	public void userRemove(
			@WebParam(name = "token") String token,
			@WebParam(name = "userId") String userId
	) throws AccessForbiddenException, InterruptedOperationException;

}
