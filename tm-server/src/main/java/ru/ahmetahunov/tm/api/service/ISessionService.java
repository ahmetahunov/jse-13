package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;

public interface ISessionService extends IAbstractService<Session> {

	@Nullable
	public Session findOne(String id, String userId) throws InterruptedOperationException;

	public void remove(String id, String userId) throws InterruptedOperationException;

	public void validate(Session session) throws AccessForbiddenException, InterruptedOperationException;

	public void validate(Session session, Role role) throws AccessForbiddenException, InterruptedOperationException;

}
