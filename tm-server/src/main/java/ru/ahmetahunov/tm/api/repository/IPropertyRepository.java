package ru.ahmetahunov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Property;
import ru.ahmetahunov.tm.exception.IdCollisionException;

public interface IPropertyRepository {

	public void persist(@NotNull Property property) throws IdCollisionException;

	public void merge(@NotNull Property property);

	@Nullable
	public Property findOne(@NotNull String propertyName);

	@Nullable
	public Property remove(@NotNull String propertyName);

	public void clear();

}
