package ru.ahmetahunov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface TaskEndpoint {

	@Nullable
	@WebMethod
	public Task createTask(
			@WebParam(name = "token") String token,
			@WebParam(name = "task") Task task
	) throws AccessForbiddenException, InterruptedOperationException;

	@Nullable
	@WebMethod
	public Task updateTask(
			@WebParam(name = "token") String token,
			@WebParam(name = "task") Task task
	) throws AccessForbiddenException, InterruptedOperationException;

	@NotNull
	@WebMethod
	public List<Task> findAllTasks(
			@WebParam(name = "token") String token,
			@WebParam(name = "comparator") String comparator
	) throws AccessForbiddenException, InterruptedOperationException;

	@NotNull
	@WebMethod
	public List<Task> findAllProjectTasks(
			@WebParam(name = "token") String token,
			@WebParam(name = "projectId") String projectId
	) throws AccessForbiddenException, InterruptedOperationException;

	@Nullable
	@WebMethod
	public Task findOneTask(
			@WebParam(name = "token") String token,
			@WebParam(name = "taskId") String taskId
	) throws AccessForbiddenException, InterruptedOperationException;

	@NotNull
	@WebMethod
	public List<Task> findTaskByName(
			@WebParam(name = "token") String token,
			@WebParam(name = "taskName") String taskName
	) throws AccessForbiddenException, InterruptedOperationException;

	@NotNull
	@WebMethod
	public List<Task> findTaskByDescription(
			@WebParam(name = "token") String token,
			@WebParam(name = "description") String description
	) throws AccessForbiddenException, InterruptedOperationException;

	@NotNull
	@WebMethod
	public List<Task> findTaskByNameOrDesc(
			@WebParam(name = "token") String token,
			@WebParam(name = "searchPhrase") String searchPhrase
	) throws AccessForbiddenException, InterruptedOperationException;

	@WebMethod
	public void removeAllTasks(
			@WebParam(name = "token") String token
	) throws AccessForbiddenException, InterruptedOperationException;

	@WebMethod
	public void removeTask(
			@WebParam(name = "token") String token,
			@WebParam(name = "taskId") String taskId
	) throws AccessForbiddenException, InterruptedOperationException;

}
