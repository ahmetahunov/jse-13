package ru.ahmetahunov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Project;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO app_project (id, name, description, startDate, finishDate, creationDate, status, user_id) " +
            "VALUES (#{id}, #{name}, #{description}, #{startDate}, #{finishDate}, #{creationDate}, #{status}, #{userId})")
    public void persist(@NotNull Project project) throws SQLException;

    @Update("UPDATE app_project SET name = #{name} , description = #{description}, startDate = #{startDate}, " +
            "finishDate = #{finishDate}, status = #{status} WHERE user_id = #{userId} AND id = #{id}")
    public void merge(@NotNull Project project) throws SQLException;

    @Nullable
    @Result(property = "userId", column = "user_id")
    @Select("SELECT * FROM app_project WHERE id = #{id}")
    public Project findOne(@NotNull @Param("id") String id) throws SQLException;

    @Nullable
    @Result(property = "userId", column = "user_id")
    @Select("SELECT * FROM app_project WHERE user_id = #{userId} AND id = #{name}")
    public Project findOneByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String projectName
    ) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_project")
    @Result(property = "userId", column = "user_id")
    public List<Project> findAll() throws SQLException;

    @NotNull
    @Result(property = "userId", column = "user_id")
    @Select("SELECT * FROM app_project WHERE user_id = #{userId}")
    public List<Project> findAllByUserId(@NotNull @Param("userId") String userId) throws SQLException;

    @NotNull
    @Result(property = "userId", column = "user_id")
    @Select("SELECT * FROM app_project WHERE user_id = #{userId} ORDER BY ${comparator}")
    public List<Project> findAllWithComparator(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("comparator") String comparator
    ) throws SQLException;

    @NotNull
    @Result(property = "userId", column = "user_id")
    @Select("SELECT * FROM app_project WHERE user_id = #{userId} AND name LIKE #{name} ORDER BY name")
    public List<Project> findByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String projectName
    ) throws SQLException;

    @NotNull
    @Result(property = "userId", column = "user_id")
    @Select("SELECT * FROM app_project WHERE user_id = #{userId} AND " +
            "description LIKE #{description} ORDER BY description")
    public List<Project> findByDescription(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("description") String description
    ) throws SQLException;

    @NotNull
    @Result(property = "userId", column = "user_id")
    @Select("SELECT * FROM app_project WHERE user_id = #{userId} " +
            "AND (name LIKE #{phrase} OR description LIKE #{phrase})")
    public List<Project> findByNameOrDesc(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("phrase") String searchPhrase
    ) throws SQLException;

    @Delete("DELETE FROM app_project WHERE user_id = #{userId}")
    public void removeAll(@NotNull @Param("userId") String userId) throws SQLException;

    @Delete("DELETE FROM app_project WHERE id = #{id}")
    public void removeById(@NotNull @Param("id") String id) throws SQLException;

    @Delete("DELETE FROM app_project WHERE user_id = #{userId} AND id = #{id}")
    public void remove(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    ) throws SQLException;

    @Insert({
            "<script>",
            "INSERT INTO app_project (id, name, description, startDate, finishDate, creationDate, status, user_id) ",
            "VALUES ",
            "<foreach  collection='data' item='project' separator=','>",
            "(#{project.id}, #{project.name}, #{project.description}, #{project.startDate}," +
                    " #{project.finishDate}, #{project.creationDate}, #{project.status}, #{project.userId})",
            "</foreach>",
            "</script>"
    })
    public void load(@NotNull @Param("data") Collection<Project> data) throws SQLException;

}
