package ru.ahmetahunov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface UserEndpoint {

	@Nullable
	@WebMethod
	public User createUser(
			@WebParam(name = "login") String login,
			@WebParam(name = "password") String password
	) throws AccessForbiddenException, InterruptedOperationException;

	@WebMethod
	public void updatePassword(
			@WebParam(name = "token") String token,
			@WebParam(name = "old") String oldPassword,
			@WebParam(name = "password") String password
	) throws AccessForbiddenException, InterruptedOperationException;

	@WebMethod
	public void updateLogin(
			@WebParam(name = "token") String token,
			@WebParam(name = "login") String login
	) throws AccessForbiddenException, InterruptedOperationException;

	@Nullable
	@WebMethod
	public User findUser(
			@WebParam(name = "token") String token
	) throws AccessForbiddenException, InterruptedOperationException;

}
