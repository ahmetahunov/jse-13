package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.AbstractEntity;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import java.util.List;

public interface IAbstractService<T extends AbstractEntity> {

    public void persist(T item) throws InterruptedOperationException;

    public void merge(T item) throws InterruptedOperationException;

    @Nullable
    public T findOne(String id) throws InterruptedOperationException;

    @NotNull
    public List<T> findAll() throws InterruptedOperationException;

    public void remove(String id) throws InterruptedOperationException;

}
