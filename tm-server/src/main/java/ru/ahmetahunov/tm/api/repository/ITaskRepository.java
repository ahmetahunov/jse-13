package ru.ahmetahunov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Task;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO app_task (id, name, description, startDate, finishDate, creationDate, status, " +
            "project_id, user_id) VALUES (#{id}, #{name}, #{description}, #{startDate}, #{finishDate}, " +
            "#{creationDate}, #{status}, #{projectId}, #{userId})")
    public void persist(@NotNull Task Task) throws SQLException;

    @Update("UPDATE app_task SET name = #{name}, description = #{description}, startDate = #{startDate}," +
            " finishDate = #{finishDate}, status = #{status}, project_id = #{projectId} " +
            "WHERE user_id = #{userId} AND id = #{id}")
    public void merge(@NotNull Task Task) throws SQLException;

    @Nullable
    @Select("SELECT * FROM app_task WHERE id = #{id}")
    @Results( value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    public Task findOne(@NotNull @Param("id") String id) throws SQLException;

    @Nullable
    @Results( value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Select("SELECT * FROM app_task WHERE user_id = #{userId} AND id = #{id}")
    public Task findOneById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String taskId
    ) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_task")
    @Results( value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    public List<Task> findAll() throws SQLException;
    
    @NotNull
    @Select("SELECT * FROM app_task WHERE user_id = #{userId}")
    @Results( value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    public List<Task> findAllByUserId(@NotNull @Param("userId") String userId) throws SQLException;

    @NotNull
    @Results( value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Select("SELECT * FROM app_task WHERE user_id = #{userId} ORDER BY ${comparator}")
    public List<Task> findAllWithComparator(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("comparator") String comparator
    ) throws SQLException;

    @NotNull
    @Results( value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Select("SELECT * FROM app_task WHERE user_id = #{userId} AND project_id = #{projectId} ORDER BY ${comparator}")
    public List<Task> findAllByProjectId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId,
            @NotNull @Param("comparator") String comparator
    ) throws SQLException;

    @NotNull
    @Results( value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Select("SELECT * FROM app_task WHERE user_id = #{userId} AND name LIKE #{name} ORDER BY name")
    public List<Task> findByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String taskName
    ) throws SQLException;

    @NotNull
    @Results( value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Select("SELECT * FROM app_task WHERE user_id = #{userId} AND name LIKE #{phrase} " +
            "OR description LIKE #{phrase} ORDER BY name")
    public List<Task> findByNameOrDesc(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("phrase") String searchPhrase
    ) throws SQLException;

    @NotNull
    @Results( value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Select("SELECT * FROM app_task WHERE user_id = #{userId} " +
            "AND description LIKE #{description} ORDER BY description")
    public List<Task> findByDescription(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("description") String description
    ) throws SQLException;

    @Delete("DELETE FROM app_task WHERE id = #{id}")
    public void remove(@NotNull @Param("id") String id) throws SQLException;

    @Delete("DELETE FROM app_task WHERE user_id = #{userId} AND id = #{id}")
    public void removeById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String taskId
    ) throws SQLException;

    @Delete("DELETE FROM app_task WHERE user_id = #{userId}")
    public void removeAll(@NotNull @Param("userId") String userId) throws SQLException;

    @Insert({
            "<script>",
            "INSERT INTO app_task (id, name, description, startDate, finishDate, creationDate, " +
                    "status, project_id, user_id) ",
            "VALUES ",
            "<foreach  collection='data' item='task' separator=','>",
            "(#{task.id}, #{task.name}, #{task.description}, #{task.startDate}, #{task.finishDate}," +
                    " #{task.creationDate}, #{task.status}, #{task.projectId}, #{task.userId})",
            "</foreach>",
            "</script>"
    })
    public void load(@NotNull @Param("data") Collection<Task> data) throws SQLException;

}
