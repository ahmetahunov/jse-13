package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import java.util.Collection;
import java.util.List;

public interface IProjectService extends IAbstractService<Project> {

    @Nullable
    public Project findOne(String userId, String projectId) throws InterruptedOperationException;

    @NotNull
    public List<Project> findByName(String userId, String projectName) throws InterruptedOperationException;

    @NotNull
    public List<Project> findByDescription(String userId, String description) throws InterruptedOperationException;

    @NotNull
    public List<Project> findByNameOrDesc(String userId, String searchPhrase) throws InterruptedOperationException;

    @NotNull
    public List<Project> findAll(String userId) throws InterruptedOperationException;

    @NotNull
    public List<Project> findAll(String userId, String comparator) throws InterruptedOperationException;

    public void removeAll(String userId) throws InterruptedOperationException;

    public void remove(String userId, String projectId) throws InterruptedOperationException;

    public void load(Collection<Project> data) throws InterruptedOperationException;

}
