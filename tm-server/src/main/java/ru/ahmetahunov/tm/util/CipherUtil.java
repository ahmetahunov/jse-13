package ru.ahmetahunov.tm.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;

public class CipherUtil {

	@NotNull
	private static SecretKeySpec setKey(@NotNull final String myKey) throws Exception {
		@NotNull byte[] key = myKey.getBytes("UTF-8");
		@NotNull final MessageDigest sha = MessageDigest.getInstance("SHA-1");
		key = sha.digest(key);
		key = Arrays.copyOf(key, 16);
		return new SecretKeySpec(key, "AES");
	}

	@Nullable
	public static String encrypt(
			@Nullable final Session toEncrypt,
			@Nullable final String secret
	) throws AccessForbiddenException {
		try {
			if (toEncrypt == null) return null;
			if (secret == null || secret.isEmpty()) return null;
			@NotNull final ObjectMapper objectMapper = new ObjectMapper();
			@NotNull final String json = objectMapper.writeValueAsString(toEncrypt);
			@NotNull final SecretKeySpec secretKey = setKey(secret);
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			byte[] data = cipher.doFinal(json.getBytes("UTF-8"));
			return Base64.getEncoder().encodeToString(data);
		} catch (Exception e) {
			e.printStackTrace();
			throw new AccessForbiddenException();
		}
	}

	@Nullable
	public static Session decrypt(
			@Nullable final String toDecrypt,
			@Nullable final String secret
	) throws AccessForbiddenException {
		try {
			if (toDecrypt == null || toDecrypt.isEmpty()) return null;
			if (secret == null || secret.isEmpty()) return null;
			@NotNull final ObjectMapper objectMapper = new ObjectMapper();
			@NotNull final SecretKeySpec secretKey = setKey(secret);
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			byte[] data = Base64.getDecoder().decode(toDecrypt);
			return objectMapper.readValue(new String(cipher.doFinal(data)), Session.class);
		} catch (Exception e) {
			e.printStackTrace();
			throw new AccessForbiddenException();
		}
	}

}
