package ru.ahmetahunov.tm.command.system;

import com.jcabi.manifests.Manifests;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show information about application.";
    }

    @Override
    public void execute() {
        @NotNull final StringBuilder builder = new StringBuilder();
        builder.append("TaskManager");
        builder.append("\nbuild: ");
        builder.append(Manifests.read("buildNumber"));
        builder.append("\nDeveloped by: ");
        builder.append(Manifests.read("developer"));
        builder.append("\nemail: ");
        builder.append(Manifests.read("email"));
        serviceLocator.getTerminalService().writeMessage(builder.toString());
    }

}
