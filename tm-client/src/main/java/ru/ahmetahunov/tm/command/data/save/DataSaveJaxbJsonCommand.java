package ru.ahmetahunov.tm.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.command.AbstractCommand;

@NoArgsConstructor
public class DataSaveJaxbJsonCommand extends AbstractCommand {

	@NotNull
	@Override
	public String getName() {
		return"data-save-jaxb-json";
	}

	@NotNull
	@Override
	public String getDescription() {
		return "Save all repositories like json file (JAXB).";
	}

	@Override
	public void execute() throws Exception {
		@Nullable final String session = serviceLocator.getStateService().getSession();
		serviceLocator.getAdminEndpoint().dataSaveJsonJaxb(session);
		serviceLocator.getTerminalService().writeMessage("[SAVED]");
	}

}
