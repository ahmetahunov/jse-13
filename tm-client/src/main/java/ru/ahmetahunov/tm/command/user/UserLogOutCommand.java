package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class UserLogOutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "log-out";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Log out.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String session = serviceLocator.getStateService().getSession();
        terminalService.writeMessage("[LOG OUT]");
        serviceLocator.getSessionEndpoint().removeSession(session);
        serviceLocator.getStateService().setSession(null);
        terminalService.writeMessage("Have a nice day!");
    }

}
