package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.User;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.util.InfoUtil;

@NoArgsConstructor
public final class UserProfileAdminCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "show-user-profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show selected user's profile.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String session = serviceLocator.getStateService().getSession();
        terminalService.writeMessage("[USER PROFILE]");
        @NotNull final String login = terminalService.getAnswer("Please enter user login: ");
        @Nullable final User user = serviceLocator.getAdminEndpoint().userFindByLogin(session, login);
        terminalService.writeMessage(InfoUtil.getUserInfo(user));
    }

}
