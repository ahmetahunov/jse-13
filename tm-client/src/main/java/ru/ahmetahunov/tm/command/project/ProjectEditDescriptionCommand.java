package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.exception.FailedOperationException;
import java.lang.Exception;

@NoArgsConstructor
public final class ProjectEditDescriptionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-edit-desc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit description of project";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = serviceLocator.getStateService().getSession();
        @NotNull final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[EDIT DESCRIPTION]");
        @NotNull final String projectId = terminalService.getAnswer("Please enter project id: ");
        @Nullable final Project project = projectEndpoint.findOneProject(session, projectId);
        if (project == null) throw new FailedOperationException("Selected project does not exist");
        @NotNull final String description = terminalService.getAnswer("Please enter new description: ");
        project.setDescription(description);
        projectEndpoint.updateProject(session, project);
        terminalService.writeMessage("[OK]");
    }

}
