package ru.ahmetahunov.tm.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.command.AbstractCommand;

@NoArgsConstructor
public class DataSaveJacksonXmlCommand extends AbstractCommand {

	@NotNull
	@Override
	public String getName() {
		return"data-save-jackson-xml";
	}

	@NotNull
	@Override
	public String getDescription() {
		return "Save all repositories like xml file (Jackson).";
	}

	@Override
	public void execute() throws Exception {
		@Nullable final String session = serviceLocator.getStateService().getSession();
		serviceLocator.getAdminEndpoint().dataSaveXmlJackson(session);
		serviceLocator.getTerminalService().writeMessage("[SAVED]");
	}

}
