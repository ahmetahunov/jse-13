package ru.ahmetahunov.tm.command.data.load;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.command.AbstractCommand;

@NoArgsConstructor
public class DataLoadJacksonXmlCommand extends AbstractCommand {

	@NotNull
	@Override
	public String getName() {
		return "data-load-jackson-xml";
	}

	@NotNull
	@Override
	public String getDescription() {
		return "Load data from xml file using Jackson.";
	}

	@Override
	public void execute() throws Exception {
		@Nullable final String session = serviceLocator.getStateService().getSession();
		serviceLocator.getAdminEndpoint().dataLoadXmlJackson(session);
		serviceLocator.getTerminalService().writeMessage("[LOAD COMPLETE]");
	}

}
