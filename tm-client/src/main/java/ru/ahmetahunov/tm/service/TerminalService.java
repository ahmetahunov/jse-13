package ru.ahmetahunov.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import java.io.BufferedReader;
import java.io.IOException;

@RequiredArgsConstructor
public final class TerminalService implements ITerminalService {

    @NotNull
    private final BufferedReader reader;

    @NotNull
    @Override
    public String readMessage() throws IOException { return reader.readLine().trim(); }

    @Override
    public void writeMessage(@NotNull final String message) {
        System.out.println(message);
    }

    @NotNull
    @Override
    public String getAnswer(@NotNull final String question) throws IOException {
        System.out.print(question);
        return reader.readLine().trim();
    }

    @Override
    public void close() throws IOException { reader.close(); }

}
