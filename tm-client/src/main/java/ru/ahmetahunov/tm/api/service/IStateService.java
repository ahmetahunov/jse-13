package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.command.AbstractCommand;
import java.util.Collection;

public interface IStateService {

    @NotNull
    public Collection<AbstractCommand> getCommands();

    @Nullable
    public String getSession();

    public void setSession(String session);

    @Nullable
    public AbstractCommand getCommand(String operation);

}
